from DemoStreamListener import DemoStreamListener
from Constants import API_KEY, API_SECRET_KEY, ACCESS_TOKEN, ACCESS_TOKEN_SECRET
import tweepy

# authorize the API Key
authentication = tweepy.OAuthHandler(API_KEY, API_SECRET_KEY)

# authorization to user's access token and access token secret
authentication.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

# call the api
api = tweepy.API(authentication)

stream_listener = DemoStreamListener()
myStream = tweepy.Stream(auth=api.auth, listener=stream_listener,
                         tweet_mode="extended")

myStream.filter(track=['Covid', 'covid-19'])
