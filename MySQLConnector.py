import pymysql


class MySQlConnector:
    def __init__(self):
        self.db = pymysql.connect('localhost', 'root', 'root', 'twitter_stream')
        self.cursor = self.db.cursor()

    def execute_cmd(self, command):
        self.cursor.execute(command)
        self.db.commit()

    def execute_select_cmd(self, command):
        self.cursor.execute(command)
        return self.cursor.fetchall()

    def close_connection(self):
        self.db.close()
