API_KEY = 'XXXXXX'

API_SECRET_KEY = 'XXXXXX'

BEARER_TOKEN = 'XXXXXX'

ACCESS_TOKEN = 'XXXXXX'

ACCESS_TOKEN_SECRET = 'XXXXXX'

INSERT_USER_CMD = 'INSERT IGNORE INTO twitter_user (user_id, user_name) VALUES ({user_id}, "{user_name}")'

INSERT_TWEET_CMD = 'INSERT INTO twitter_tweet (tweet_id, user_id, tweet, retweet_count) VALUES ({tweet_id}, {user_id}, "{tweet}", {retweet_count})'

INSERT_TWEET_ENTITY_CMD = 'INSERT INTO twitter_entity (tweet_id, hash_tag) VALUES({tweet_id}, "{hash_tag}")'

SELECT_ALL_TWEET = 'select tweet_id, user_id, tweet from twitter_tweet'

SELECT_ALL_HASH = 'select tweet_id, hash_tag from twitter_entity'
