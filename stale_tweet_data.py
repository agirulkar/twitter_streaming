from Constants import API_KEY, API_SECRET_KEY, ACCESS_TOKEN, ACCESS_TOKEN_SECRET, INSERT_USER_CMD
import tweepy

# authorize the API Key
authentication = tweepy.OAuthHandler(API_KEY, API_SECRET_KEY)

# authorization to user's access token and access token secret
authentication.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

# call the api
api = tweepy.API(authentication)

# Get personal timeline tweets.
public_tweet = api.home_timeline(count=5)
for tweet in public_tweet:
    print("===>", tweet.text)

# Get specified user's tweets
user_timeline = api.user_timeline(id='ABPNews', count=2)
for tweet in user_timeline:
    print("==>", tweet.text)

result = api.search(['COVID', 'covid-19', 'COVID-19', 'CORONA'], lang='en', count=10)
for item in result:
    print(item.entities['hashtags'])
