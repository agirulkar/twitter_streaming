from nltk.stem import WordNetLemmatizer
import re
from MySQLConnector import MySQlConnector
from Constants import SELECT_ALL_TWEET, SELECT_ALL_HASH
import pandas as pd
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from textblob import TextBlob
import seaborn as sns


class Analysis:
    def __init__(self):
        self.mysql_obj = MySQlConnector()

    # Pre processing to get the clean tweets
    def pre_processing(self, tweet_txt):
        # remove links
        tweet_txt = re.sub(r'http\S+', '', tweet_txt)

        # remove mentions
        tweet_txt = re.sub('@\w+', '', tweet_txt)

        # Remove non-alphanumeric and hashtags
        tweet_txt = re.sub('[^a-zA-Z]', ' ', tweet_txt)

        # remove multiple spaces
        tweet_txt = re.sub('\s+', ' ', tweet_txt)

        tweet_txt = tweet_txt.lower()

        lemmatizer = WordNetLemmatizer()
        sent = ' '.join([lemmatizer.lemmatize(w) for w in tweet_txt.split() if len(lemmatizer.lemmatize(w)) > 3])
        print(sent)
        return sent

    # Draw the word cloud with specified data
    def draw_word_cloud(self, df_tweet):
        all_words = ' '.join([text for text in df_tweet['clean_tweet']])
        wordcloud = WordCloud(width=1366, height=800, random_state=21, max_font_size=110).generate(all_words)

        plt.figure(figsize=(10, 7))
        plt.imshow(wordcloud, interpolation="bilinear")
        plt.axis('off')
        plt.show()

    # Get whether the tweet is positive negative or neutral
    def sentiment_analysis(self, tweet):
        analysis = TextBlob(tweet)
        if analysis.sentiment.polarity > 0:
            return 1
        elif analysis.sentiment.polarity == 0:
            return 0
        else:
            return -1

    def draw_bar_graph(self, pivot_df):
        graph_data = pivot_df.nlargest(columns="tweet_id", n=10)

        # Creating bar graph
        plt.figure(figsize=(12, 5))
        ax = sns.barplot(data=graph_data, x="hash_tag", y="tweet_id", palette="Reds_d")

        # Altering the visual elements
        sns.set_context("paper")
        ax.set(ylabel='Count')
        ax.set_xticklabels(labels=ax.get_xticklabels(), rotation=70, fontsize=7)

        plt.title('#Hashtags')

        # Output plot
        plt.show()


if __name__ == "__main__":
    analysis = Analysis()
    # result_set = analysis.mysql_obj.execute_select_cmd(SELECT_ALL_TWEET)
    result_set_hashtags = analysis.mysql_obj.execute_select_cmd(SELECT_ALL_HASH)
    tweet_df = pd.DataFrame(columns=['tweet_id', 'user_id', 'clean_tweet'])
    hash_df = pd.DataFrame(columns=['tweet_id', 'hash_tag'])
    for data in result_set:
        index = len(tweet_df)
        tweet_df.loc[index, 'tweet_id'] = data[0]
        tweet_df.loc[index, 'user_id'] = data[1]
        tweet_df.loc[index, 'clean_tweet'] = analysis.pre_processing(data[2])

    for data in result_set_hashtags:
        index = len(hash_df)
        hash_df.loc[index, 'tweet_id'] = data[0]
        hash_df.loc[index, 'hash_tag'] = data[1]

    analysis.draw_word_cloud(tweet_df)
    tweet_df['Sentiment'] = tweet_df['clean_tweet'].apply(analysis.sentiment_analysis)
    print(tweet_df.head(20))

    # Unique hashtag counts
    table = hash_df.pivot_table(index="hash_tag", values='tweet_id', aggfunc=len)

    # Convert pivot table to dataframe
    df_pivot = pd.DataFrame(table.to_records())
    analysis.draw_bar_graph(df_pivot)
