import tweepy
import time
from MySQLConnector import MySQlConnector
from Constants import INSERT_TWEET_CMD, INSERT_USER_CMD, INSERT_TWEET_ENTITY_CMD


class DemoStreamListener(tweepy.StreamListener):
    def __init__(self, time_limit=20):
        self.start_time = time.time()
        self.limit = time_limit
        self.mysql_obj = MySQlConnector()
        super(DemoStreamListener, self).__init__()

    def on_connect(self):
        print('Connected twitter API')

    def on_status(self, status):
        tweet_id = status.id
        user_id = status.user.id
        user_name = status.user.name

        # If tweet is extended after 140 chars
        if status.truncated:
            tweet = status.extended_tweet['full_text'].strip().replace('"', '').replace("'", "")
            hashtags = status.extended_tweet['entities']['hashtags']
        else:
            tweet = status.text.strip().replace('"', '').replace("'", "")
            hashtags = status.entities['hashtags']

        # Read hastags
        hashtags = [item['text'] for item in hashtags]

        # Re-tweet count
        retweet_count = status.retweet_count

        # Language
        lang = status.lang

        # If tweet is not a re-tweet and tweet is in English
        if not hasattr(status, "retweeted_status") and lang == "en":

            print(tweet_id, user_id, user_name, tweet, retweet_count, hashtags)
            insert_user_cmd = INSERT_USER_CMD.format(user_id=user_id, user_name=user_name)
            print(insert_user_cmd)
            self.mysql_obj.execute_cmd(insert_user_cmd)

            insert_tweet_cmd = INSERT_TWEET_CMD.format(tweet_id=tweet_id, user_id=user_id, tweet=tweet,
                                                       retweet_count=retweet_count)
            print(insert_tweet_cmd)
            self.mysql_obj.execute_cmd(insert_tweet_cmd)

            for hash_tag in hashtags:
                insert_tweet_entity_cmd = INSERT_TWEET_ENTITY_CMD.format(tweet_id=tweet_id, hash_tag=hash_tag)
                self.mysql_obj.execute_cmd(insert_tweet_entity_cmd)

        if (time.time() - self.start_time) > self.limit:
            print(time.time(), self.start_time, self.limit)
            self.mysql_obj.close_connection()
            return False

    def on_error(self, status_code):
        if status_code == 420:
            # Returning False in on_data disconnects the stream
            self.mysql_obj.close_connection()
            return False
